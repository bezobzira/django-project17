/**
 * =====================
 * 1. BASIC GULP OPTIONS
 * =====================
 *
 * Restart Gulp if making changes to the settings
 *
 * LIVERELOAD_BROWSER_ON_SAVE requires browser plugin
 *
 * Paths are relative to one level above gulpfile.js ('../')
 *
 * LIVERELOAD_BROWSER_ON_SAVE: @type {bool}
 * OUTPUT_DIR: @type {string}
 * SOURCE_DIR: @type {string}
 * LIBRARY_DIR: @type {string}
 */

LIVERELOAD_BROWSER_ON_SAVE = true;

OUTPUT_DIR = 'static';

SOURCE_DIR = 'client';

LIBRARY_DIR = 'client/lib';


/**
 * ======================
 * 2. SOURCE FILE OPTIONS
 * ======================
 *
 * Javascript, LESS and CSS files are concatenated in alphabetical order level by level, shallow > deep
 * Images are optimized and copied
 * All other files are only copied
 *
 * Use concatenation order lists to specify priority files and/or folders
 * Use exclusion list to specify ignored files and/or folders
 *
 * Paths are relative to SOURCE_DIR
 *
 * JAVASCRIPT_CONCATENATION_ORDER: @type {string[]}
 * LESS_AND_CSS_CONCATENATION_ORDER: @type {string[]}
 * EXCLUDED_FILES_AND_FOLDERS: @type {string[]}
 */

JAVASCRIPT_CONCATENATION_ORDER = [
	
];

LESS_AND_CSS_CONCATENATION_ORDER = [

];

EXCLUDED_FILES_AND_FOLDERS = [

];


/**
 * ==================
 * 3. LIBRARY OPTIONS
 * ==================
 *
 * Only specified files are considered
 * List order = concatenation order
 *
 * Restart Gulp if making changes to lib source files
 *
 * Paths are relative to LIBRARY_DIR¹
 *
 * ¹LIBRARY_DIR automatically gets excluded from non-library tasks' source paths if inside SOURCE_DIR
 * (as if it were prepended to EXCLUDED_FILES_AND_FOLDERS list)
 *
 * JAVASCRIPT_LIBS: @type {string[]}
 * LESS_AND_CSS_LIBS: @type {string[]}
 */

JAVASCRIPT_LIBS = [
    'core/jquery.min.js'
];

LESS_AND_CSS_LIBS = [

];