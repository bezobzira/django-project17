'use strict';

var chalk = require('chalk');
var time = require('pretty-hrtime');
var tildify = require('tildify');
var glob = require('glob');

var log = {
    info: function (message) {
        console.log('[' + chalk.grey(new Date().toString().substring(16, 24)) + '] ' + message);
    },
    callback: function (task, start) {
        console.log(
            '[' + chalk.grey(new Date().toString().substring(16, 24)) + '] ' +
            'Callback \'' + chalk.cyan(task) + '\' after ' +
            chalk.magenta(time(process.hrtime(start)))
        );
    },
    path: function (options) {
        if (options && options.name && options.path) {
            console.log(
                '[' + chalk.grey(new Date().toString().substring(16, 24)) + '] ' +
                'Using ' + options.name + ' ' +
                chalk.magenta(tildify(glob.sync(options.path, {realpath: true})[0]))
            );
        }
    },
    timer: function (message, start) {
        console.log(
            '[' + chalk.grey(new Date().toString().substring(16, 24)) + '] ' +
            message + ' after ' + chalk.magenta(time(process.hrtime(start)))
        );
    }
};

function formatSlashes(path) {
    if (path[0] === '/') {
        path = path.substring(1);
    }

    if (path.split('.').length === 1 && path[path.length - 1] !== '/') {
        path += '/';
    }

    return path;
}

function determinePathType(path, extension) {
    if (path.split('.').length === 1) {
        path += '**/*' + (extension && typeof extension === 'string' ? extension : '');
    }

    return path;
}

function isBoolean(option) {
    if (typeof option !== 'boolean') {
        throw new Error('Invalid main option: type must be Boolean');
    }
}

function isString(option, type) {
    if (typeof option !== 'string') {
        throw new Error('Invalid main option: type must be String');
    }

    if (option.length < 1) {
        throw new Error('Invalid main option: String must not be empty');
    }

    if (type && type.isPath && option[0].match(/[^a-zA-Z/]/g)) {
        throw new Error('Invalid main option: String must start with a letter or a forward slash');
    }

    if (type && type.isDir && option.split('.').length > 1) {
        throw new Error('Ivalid main option: String must be directory path');
    }
}

function isArrayOfStrings(option, type) {
    if (!option || typeof option !== 'object' || option.constructor !== Array) {
        throw new Error('Invalid main option: type must be Array');
    }

    if (option.length > 0) {
        option.forEach(function (item) {
            if (typeof item !== 'string') {
                throw new Error('Invalid Array suboption: type must be String');
            }

            if (item.length < 1) {
                throw new Error('Invalid Array suboption: String must not be empty');
            }

            if (type && type.isPath && item[0].match(/[^a-zA-Z/]/g)) {
                throw new Error('Invalid Array suboption: String must start with a letter or a forward slash');
            }
        });
    }
}

module.exports = {
    log: log,
    formatSlashes: formatSlashes,
    determinePathType: determinePathType,
    isBoolean: isBoolean,
    isString: isString,
    isArrayOfStrings: isArrayOfStrings
};