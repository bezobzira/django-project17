# GULP INSTRUCTIONS #

1. Navigate to **gulp** dir
1. Run  `npm install` to install Gulp dependencies defined in the [package.json](package.json) file
1. Edit options in the [settings.js](../settings.js) file as needed
1. Run `gulp` to start Gulp tasks