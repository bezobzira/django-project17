'use strict';

// Load verification tools
var gulpify = require('./gulpify');
var checkpoint;

// Load data from the settings.js file
checkpoint = process.hrtime();

require('../settings');

gulpify.log.path({
    name: 'settings file',
    path: '../settings.js'
});

/*
 Check if provided options have correct types:

 LIVERELOAD_BROWSER_ON_SAVE: @type {bool}
 OUTPUT_DIR: @type {string}
 SOURCE_DIR: @type {string}
 LIBRARY_DIR: @type {string}
 JAVASCRIPT_CONCATENATION_ORDER: @type {string[]}
 LESS_AND_CSS_CONCATENATION_ORDER: @type {string[]}
 EXCLUDED_FILES_AND_FOLDERS: @type {string[]}
 JAVASCRIPT_LIBS: @type {string[]}
 LESS_AND_CSS_LIBS: @type {string[]}
 JAVASCRIPT_LIB_REGISTRY: @type {{key: string}}
 AUTOPREFIXER_BROWSER_SUPPORT: @type {string[]}
 */
gulpify.isBoolean(LIVERELOAD_BROWSER_ON_SAVE);
gulpify.isString(OUTPUT_DIR, {isPath: true, isDir: true});
gulpify.isString(LIBRARY_DIR, {isPath: true, isDir: true});
gulpify.isString(SOURCE_DIR, {isPath: true, isDir: true});
gulpify.isArrayOfStrings(JAVASCRIPT_CONCATENATION_ORDER, {isPath: true});
gulpify.isArrayOfStrings(LESS_AND_CSS_CONCATENATION_ORDER, {isPath: true});
gulpify.isArrayOfStrings(EXCLUDED_FILES_AND_FOLDERS, {isPath: true});
gulpify.isArrayOfStrings(JAVASCRIPT_LIBS, {isPath: true});
gulpify.isArrayOfStrings(LESS_AND_CSS_LIBS, {isPath: true});

gulpify.log.timer('Settings verified', checkpoint);

checkpoint = process.hrtime();

gulpify.log.info('Building tasks...');

// Load basic gulp plugins
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var insert = require('gulp-insert');
var _if = require('gulp-if');
var rename = require('gulp-rename');
//var gzip = require('gulp-gzip');
var del = require('del');

if (LIVERELOAD_BROWSER_ON_SAVE) {
    var livereload = require('gulp-livereload');
}

// Prepare namespaces for paths and tasks
var tasks = [];
var paths = {
    dist: '../' + gulpify.formatSlashes(OUTPUT_DIR),
    src: '../' + gulpify.formatSlashes(SOURCE_DIR),
    less: [],
    js: [],
    copy: [],
    lib: {
        src: '../' + gulpify.formatSlashes(LIBRARY_DIR),
        js: [],
        css: []
    }
};


/*
 Build source paths for the copy task:
 Include all files and folders,
 except files whose extensions match one of specified extensions.
 */
paths.copy.push(paths.src + '**/*.*');
paths.copy.push('!' + paths.src + '**/*.{js,less,css,jpg,jpeg,png,gif,svg}');

/*
 Build source paths for the less task:
 First include files and folders from the list,
 then add all other less and css files.
 */
if (LESS_AND_CSS_CONCATENATION_ORDER.length > 0) {
    LESS_AND_CSS_CONCATENATION_ORDER.forEach(function (path) {
        paths.less.push(paths.src + gulpify.determinePathType(gulpify.formatSlashes(path), '.{less,css}'));
    });
}

paths.less.push(paths.src + '**/*.{less,css}');

/*
 Build source paths for the javascript task:
 First include files and folders from the list,
 then add all other js files.
 */
if (JAVASCRIPT_CONCATENATION_ORDER.length > 0) {
    JAVASCRIPT_CONCATENATION_ORDER.forEach(function (path) {
        paths.js.push(paths.src + gulpify.determinePathType(gulpify.formatSlashes(path), '.js'));
    });
}

paths.js.push(paths.src + '**/*.js');

/*
 Build source paths for the images task:
 Include all images (determined by file extensions).
 */

/*
 If library dir is inside of source dir,
 exclude it from source paths of non-library related gulp tasks.
 */
if (paths.lib.src.indexOf(paths.src) >= 0) {
    [paths.less, paths.js, paths.copy].forEach(function (path) {
        path.push('!' + paths.lib.src + '**/*');
    });
}

/*
 Sort excluded files and folders from the list
 and exclude them from source paths of relevant gulp tasks.
 Folders get excluded from source paths of all tasks,
 files get excluded from source paths of relevant tasks only.
 */
if (EXCLUDED_FILES_AND_FOLDERS.length > 0) {
    var file, path;

    EXCLUDED_FILES_AND_FOLDERS.forEach(function (name) {
        file = {
            segments: name.split('.')
        };

        if (file.segments.length === 1) {
            [paths.less, paths.js, paths.copy].forEach(function (path) {
                path.push('!' + paths.src + gulpify.formatSlashes(name) + '**/*');
            });
        }
        else {
            file.extension = file.segments[file.segments.length - 1];

            switch (file.extension) {
                case 'less':
                case 'css':
                    path = paths.less;
                    break;

                case 'js':
                    path = paths.js;
                    break;

                default:
                    path = paths.copy;
            }

            path.push('!' + paths.src + gulpify.formatSlashes(name));
        }
    });
}


gulp.task('copy', function () {
    var start = process.hrtime();

    del([
        paths.dist + 'misc/**/*',
        paths.dist + 'misc'
    ], { force: true });

    gulp.src(paths.copy)
        .pipe(rename({
            dirname: ''
        }))
        .pipe(gulp.dest(paths.dist + 'misc'))
        .on('end', function () {
            gulpify.log.callback('copy', start);
        });
});

tasks.push('copy');


var lessc = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var autoprefix = require('gulp-autoprefixer');

gulp.task('less', function () {
    var start = process.hrtime();

    del([
        paths.dist + 'css/app.min.css',
        paths.dist + 'css/app.min.css.gz'
    ], { force: true });

    gulp.src(paths.less)
        .pipe(plumber())
        .pipe(concat('app.min.css'))
        .pipe(lessc({
            strictMath: true
        }))
        .pipe(autoprefix({
            cascade: false
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest(paths.dist + 'css'))
        .pipe(_if(LIVERELOAD_BROWSER_ON_SAVE, livereload()))
        //.pipe(gzip())
        .pipe(gulp.dest(paths.dist + 'css'))
        .on('end', function () {
            gulpify.log.callback('less', start);
        });
});

tasks.push('less');


LESS_AND_CSS_LIBS.forEach(function (lib) {
    paths.lib.css.push(paths.lib.src + gulpify.determinePathType(gulpify.formatSlashes(lib), '.{less,css}'));
});

gulp.task('libs-less-css', function () {
    var start = process.hrtime();

    del([
        paths.dist + 'css/**/lib.min.css',
        paths.dist + 'css/**/lib.min.css.gz'
    ], { force: true });

    gulp.src(paths.lib.css)
        .pipe(plumber())
        .pipe(_if(paths.lib.css.join().indexOf('.less') >= 0, lessc({
            strictMath: true
        })))
        .pipe(concat('lib.min.css'))
        .pipe(autoprefix({
            cascade: false,
            remove: false
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest(paths.dist + 'css'))
        .pipe(_if(LIVERELOAD_BROWSER_ON_SAVE, livereload()))
        //.pipe(gzip())
        .pipe(gulp.dest(paths.dist + 'css'))
        .on('end', function () {
            gulpify.log.callback('libs-less-css', start);
        });
});

tasks.push('libs-less-css');


var uglify = require('gulp-uglify');

gulp.task('javascript', function () {
    var start = process.hrtime();

    del([
        paths.dist + 'js/**/app.min.js',
        paths.dist + 'js/**/app.min.js.gz'
    ], { force: true });

    gulp.src(paths.js)
        .pipe(plumber())
        .pipe(concat('app.min.js'))
        .pipe(insert.wrap('(function () {\'use strict\';', '}).call(this);'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist + 'js'))
        .pipe(_if(LIVERELOAD_BROWSER_ON_SAVE, livereload()))
        //.pipe(gzip())
        .pipe(gulp.dest(paths.dist + 'js'))
        .on('end', function () {
            gulpify.log.callback('javascript', start);
        });
});

tasks.push('javascript');


JAVASCRIPT_LIBS.forEach(function (lib) {
    paths.lib.js.push(paths.lib.src + gulpify.determinePathType(gulpify.formatSlashes(lib), '.js'));
});

gulp.task('libs-javascript', function () {
    var start = process.hrtime();

    del([
        paths.dist + 'js/**/lib.min.js',
        paths.dist + 'js/**/lib.min.js.gz'
    ], { force: true });

    gulp.src(paths.lib.js)
        .pipe(plumber())
        .pipe(concat('lib.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist + 'js'))
        .pipe(_if(LIVERELOAD_BROWSER_ON_SAVE, livereload()))
        //.pipe(gzip())
        .pipe(gulp.dest(paths.dist + 'js'))
        .on('end', function () {
            gulpify.log.callback('libs-javascript', start);
        });
});

tasks.push('libs-javascript');

/*
 Main task
 1. executes all other tasks
 2. activates livereload and watchers
 */
gulp.task('default', tasks, function () {
    if (LIVERELOAD_BROWSER_ON_SAVE) {
        livereload.listen();
    }

    gulp.watch(paths.copy, ['copy']);
    gulp.watch(paths.less, ['less']);
    gulp.watch(paths.js, ['javascript']);

    if (LIVERELOAD_BROWSER_ON_SAVE) {
        gulp.watch([
            '../templates/**/*.html',
            '../apps/**/*.html',
            '../app/**/*.html'
        ]).on('change', function (file) {
            livereload.reload(file.path);
        });
    }
});

gulpify.log.timer('Tasks ready', checkpoint);